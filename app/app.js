/**
 * Definição dos módulos, controllers, rotas e diretivas da aplicação
 * @author Equipe de Desenvolvimento
 * @version 1.0
 * @copyright UFC/UNASUS (Nuteds)
 */

// Definindo namespace pcfa para uso global e adicionado os módulos que serão usados na aplicação
var pcfa1 = angular.module('pcfa1', ['ngRoute', 'ui.bootstrap']);

/*
 * Controllers
 */

// Responsável por fazer o parser do JSON
pcfa1.controller('menuController', ['$scope', '$http', '$route', function($scope, $http, $route) {
  $http.get('data/pcfa1.json')
  .success(function(data) {
    $scope.unidades = data.unidades;
  })
  .error(function(data) {
    console.log("Erro ao capturar os dados do JSON");
  });
}]);

// Responsável por injetar na view o arquivo correspondente a url requisitada.
pcfa1.controller('includeController', ['$scope', '$routeParams', function($scope, $routeParams) {
  $scope.paginaID     = $routeParams.paginaID;
  $scope.opcao        = $routeParams.opcao;
  var path            = 'views/unidade_1/'+$scope.opcao+'/'+$scope.paginaID+'.html';
  $scope.getPartial   = function () {return path;}
}]);

/*
 * Routers
 */

pcfa1.config (['$routeProvider', function ($routeProvider) {
    $routeProvider
    .when ('/', {
      templateUrl: 'views/unidade_1/sumario.html',
      controller: 'menuController'
    })
    .when ('/creditos', {
      templateUrl: 'views/curso/creditos.html'
    })
    .when ('/conclusao', {
      templateUrl: 'views/curso/conclusao.html'
    })
    .when ('/campos', {
      templateUrl: 'views/jogo/campos/index.html'
    })
    .when('/:opcao/:paginaID', {
      templateUrl: 'views/curso/include.html',
      controller: 'includeController'
    })
    .otherwise ({
      redirectTo: '/'
    });
}]);

/*
 * Directives
 */

// Responsável por injetar no DOM as informações/opções do conteúdo acessado (Ver)
pcfa1.directive('info', function() {
  return {
    templateUrl: 'partials/header.html',
    replace: true,
    restrict: 'E',
    scope: {
      unidade: "@unidade",
      atividade: "@atividade",
      unome: "@unome",
      anome: "@anome"
    }
  };
});

// Responsável pela "paginação" do curso
pcfa1.directive('paginas', function() {
  return {
    templateUrl: 'partials/pagination.html',
    replace: true,
    restrict: 'E',
    scope: {
      anterior: "@anterior",
      proximo: "@proximo",
      previous: "@previous",
      next: "@next",
      desativado: "@desativado"
    }
  }
});

// Responsável pelo gerenciamento dos modais do curso
pcfa1.directive('modal', function() {
  return {
    templateUrl: 'partials/modal.html',
    replace: true,
    restrict: 'E',
    transclude: true,
    scope: {
      target: "@target",
      titulo: "@titulo",
      tamanho: "@tamanho"
    }
  }
});