// Setando namespace/módulo global
var campos = {};

// Módulo do jogo
campos.Game = (function() {

  // Inicialização de todos os componentes do jogo
  function Field() {
    var objects = 3;
    var canvas = document.getElementById("canvas");
    var stage  = new createjs.Stage(canvas);
    createjs.Touch.enable(stage);

    // Conteúdo
    var content = [{
      "id": 0,
      "question": "Sobre a Revolução Verde no Brasil, assinale o item correto.",
      "options": ["Devido a evidente subnotificação de intoxicações, não se tem dados exatos do real impacto do uso dos agrotóxicos na saúde humana.",
      "Diferente do que aconteceu em outros países, como EUA e Japão, o consumo de agrotóxicos no Brasil foi estimulado de forma consciente e em baixa escala.",
      "Através do esclarecimento e capacitação dos agricultores sobre os riscos do uso abusivo de agrotóxicos, pode-se reduzir de forma eficiente seus impactos durante esse período."],
      "feedback": ["Parabéns! Esta é a melhor alternativa de resposta à questão. O Brasil não dispõe de dados que reflitam a realidade das intoxicações por agrotóxicos, havendo uma evidente situação de subnotificação e, para o trabalhador rural, uma constante exposição ocupacional.", "Esta não é a melhor alternativa de resposta à questão. No Brasil, a 'Revolução Verde' adquire impulso em meados da década de 1970 com a criação do Programa Nacional de Defensivos Agrícolas (PNDA). O PNDA visava estimular a produção e o consumo nacional de agrotóxicos na medida em que condicionava a concessão do crédito rural à utilização obrigatória de uma parte deste recurso com a compra de agrotóxicos (ALVES FILHO, 2002; SOARES; FREITAS; COUTINHO, 2005).", "Esta não é a melhor alternativa de resposta à questão.  A Revolução Verde não foi acompanhada por processos de qualificação dos agricultores envolvidos na produção (MOREIRA et al., 2002; SOARES; FREITAS; COUTINHO, 2005)."],
      "correct": 1,
      callback: function() {
        objects--;
        stage.removeChild(tipOne);
        success = new createjs.Bitmap(campos.graphics.success.path);
        success.x = 570;
        success.y = 413;
        stage.addChild(success);
        updateStage();
      },
      callbackWrong: function() {
        endGame();
        stage.removeChild(tipOne);
        error = new createjs.Bitmap(campos.graphics.error.path);
        error.x = 570;
        error.y = 413;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }, {
      "id": 1,
      "question": "O trabalhador rural faz uso de diferentes tipos de ferramentas que favorecem a ocorrência acidentes, tais quais citamos como foices, enxadas, facões, além do maquinário agrícola de grande porte, como os tratores e o uso de produtos químicos. Somado a isso, podemos considerar como risco ocupacional as queimaduras, intoxicações e lesões diversas.   Sobre essa proposição é correto afirmar:",
      "options": ["A eliminação da exposição ao fator de risco ou agente causal, por meio de medidas de controle ou substituição, podem assegurar a prevenção, a eliminação ou erradicação do risco.",
      "O uso de agrotóxicos na lavoura não compromete a saúde do trabalhador rural e serve como como uma ferramenta de apoio ao combate as pragas presentes nas lavouras.",
      "O uso dos equipamentos de proteção individual (EPI) não são obrigatórios pois estes não protegem a saúde do trabalhador rural e são considerados gastos desnecessários ao empregador."],     
      "feedback": ["Parabéns! Esta é a melhor alternativa de resposta à questão. O ideal é que o trabalhador rural se proteja da melhor forma possível para que elimine os riscos a que pode estar exposto no trabalho.", "Esta não é a melhor alternativa de resposta à questão. O uso incorreto de agrotóxicos é prejudicial à saúde e pode causar envenenamento.", "Esta não é a melhor alternativa de resposta à questão. Os EPI são de extrema importância para manter a integridade física do trabalhador."],
      "correct": 1,
      callback: function() {
        objects--;
        stage.removeChild(tipTwo);
        success = new createjs.Bitmap(campos.graphics.success.path);
        success.x = 132;
        success.y = 529;
        stage.addChild(success);
        updateStage();
        endGame();
      },
      callbackWrong: function() {
        stage.removeChild(tipTwo);
        error = new createjs.Bitmap(campos.graphics.error.path);
        error.x = 132;
        error.y = 529;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }, {
      "id": 2,
      "question": "Sobre a Política Nacional de Saúde Integral das Populações do Campo, da Floresta e das águas (PNSIPCF), assinale o item correto. ",
      "options": ["Visa garantir equidade, universalidade e integralidade dessa população ao SUS e foi marcado por ser elaborado sem diálogo aberto com os movimentos sociais.",
      "A Política em questão foi publicada em dezembro de 2011 com o intuito de garantir melhor assistência à saúde dessas populações, já que havia uma real desigualdade quando comparada a urbana. ",
      "O publico alvo da Política citada é a comunidade agropecuária, sendo que as outras atividades relacionadas ao campo e floresta são regidas por outra política. "],
      "feedback": ["Esta não é a melhor alternativa de resposta à questão. Elaborada com a participação do Grupo da Terra, de maneira democrática e participativa, esta política foi marcada pela relação dialógica: governo - movimentos sociais.", "Parabéns! Esta é a melhor alternativa de resposta à questão.  Os resultados dos diversos estudos sobre as condições de saúde desses grupos evidenciam um perfil mais precário quando comparadas às da população urbana. No campo, ainda existem importantes limitações de acesso e qualidade nos serviços de saúde, bem como uma situação deficiente de saneamento ambiental.", "Esta não é a melhor alternativa de resposta à questão. As populações beneficiadas diretamente pela PNSIPCF são povos e comunidades que têm seus modos de vida, produção e reprodução social relacionados predominantemente com o campo, a floresta, os ambientes aquáticos, a agropecuária e o extrativismo."],
      "correct": 2,
      callback: function() {
        objects--;
        stage.removeChild(tipThree);
        success = new createjs.Bitmap(campos.graphics.success.path);
        success.x = 150;
        success.y = 458;
        stage.addChild(success);
        updateStage();
        endGame();
      },
      callbackWrong: function() {
        stage.removeChild(tipThree);
        error = new createjs.Bitmap(campos.graphics.error.path);
        error.x = 150;
        error.y = 458;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }];

    // Preloader
    function loadGraphics(e) {
      var imagesList = [
        {name:"tip", path:"assets/images/game/tip.png"},
        {name:"landscape", path:"assets/images/game/campos.png"},
        {name:"success", path:"assets/images/game/success.png"},
        {name:"error", path:"assets/images/game/error.png"},
        {name:"end", path:"assets/images/game/end_campos.png"},
      ]

      campos.graphics = {};

      var totalFiles = imagesList.length;
      var loadedFiles = 0;
      for (var i=0, len=totalFiles; i<len; i++) {
        imageToLoad = imagesList[i];
        var img = new Image();
        img.onload = (function(event) {
          loadedFiles++;
          if (loadedFiles >= totalFiles) {
            initGame();
          }
        });
        img.src = imageToLoad.path;
        campos.graphics[imageToLoad.name] = imageToLoad;
      };
    }

    // Função auxiliar para recarregar o palco
    var updateStage = function() {
      stage.update();
    }

    // Fim do jogo
    var endGame = function() {
      if (objects == 0) {
        stage.removeAllChildren();
        updateStage();
        var end = new createjs.Bitmap(campos.graphics.end.path);
        stage.addChild(end);
        updateStage();
      }
    }

    var drawingTip = function() {
      // Rev Verde (1)
        tipOne = new createjs.Bitmap(campos.graphics.tip.path);
        tipOne.x = 570;
        tipOne.y = 413;
        stage.addChild(tipOne);
        tipOne.addEventListener("click", function() {
          modal(content[0].id, content[0].feedback, content[0].question, content[0].options);
        });
      // Leito do rio
        tipTwo = new createjs.Bitmap(campos.graphics.tip.path);
        tipTwo.x = 132;
        tipTwo.y = 529;
        stage.addChild(tipTwo);
        tipTwo.addEventListener("click", function() {
          modal(content[1].id, content[1].feedback, content[1].question, content[1].options);
        });
      // Rio
        tipThree = new createjs.Bitmap(campos.graphics.tip.path);
        tipThree.x = 150;
        tipThree.y = 458;
        stage.addChild(tipThree);
        tipThree.addEventListener("click", function() {
          modal(content[2].id, content[2].feedback, content[2].question, content[2].options);
        });
    }

    var modal = function(id, feedback, question, options, correct) {
      this.id = id;
      this.question = question;
      this.options = options;
      this.feedback = feedback;
      var indexChar = 97;
      var str = "<form id='game_form_" + this.id + "'>";
      for(var i=0;i<this.options.length;i++){
        str += "<label class='choiceStyle' style='font-weight:100 !important;'><input type='radio' name='choice' value='" + (i + 1) + "'/> " + String.fromCharCode(indexChar++) + ") " + this.options[i] + "</label><br/>";
      }
      str += "<br><input type='button' class='btn btn-sm btn-warning' value='Confirmar escolha'/></form>";
      var customModal = $('<div class="custom-modal modal fade"><div class="modal-dialog"><div class="modal-content">' +
      '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
      '<h4 class="modal-title">Questão</h4></div>' +
      '<div class="modal-body"><div class="campos-feedback"></div><p>'+ this.question +'</p>' +
      str +
      '</div>' +
      '</div></div></div>');

      $('body').append(customModal);
      $('.custom-modal').modal();

      $('.custom-modal').on('hidden.bs.modal', function(){
        $('.custom-modal').remove();
      });
      // Checando
      $("#game_form_"+ this.id +" input[type=button]").click({index: id}, function(e){
        var box = $(this).parent();
        var item = $(box).find("input[type=radio]:checked");
        var questionItem = $(item).val();
        var itemCorrect = content[e.data.index].correct;
        if(questionItem == undefined)
          return;

        if(questionItem == itemCorrect){
          $(".campos-feedback").html('<div class="alert alert-success text-justify game-tip" role="alert">'+ feedback[questionItem-1] +'</div>');
          $('.custom-modal').on('hidden.bs.modal', function(){
            $('.custom-modal').remove();
            content[e.data.index].callback();
          });
        } else {
          $(".campos-feedback").html('<div class="alert alert-danger text-justify game-tip" role="alert">'+ feedback[questionItem-1] +'</div>');
          $('.custom-modal').on('hidden.bs.modal', function(){
            $('.custom-modal').remove();
            content[e.data.index].callbackWrong();
          });
        }
        $(box).find('input').attr('disabled', true);
      });
    }

    // Fase inicial
    var initGame = function() {
      // Cenário
      var landscape = new createjs.Bitmap(campos.graphics.landscape.path);
      landscape.y = 0;
      stage.addChild(landscape);
      drawingTip();
      // Atualizar palco para carregar os elementos na tela.
      updateStage();
    }
    loadGraphics();
  }
  return Field;
})();

// Chamando o jogo
$(function() {
  var game = new campos.Game();
});