	var player;	
	var interval;
	var pitstop = [
		{
			time: 1,
		//	callback: function(){ }
		}
	];
	
	
	function onVideoFinished(){
		$("#player").hide();
        $("#informacao").show();
	}
	
	function videoSeekTo(seconds){
		player.seekTo(seconds, true);
		player.pauseVideo();	
	}
	
	function videoStop(){
		player.pauseVideo();
	}
	
	function onytplayerStateChange(newState) {
		if(newState.data == 0){
			onVideoFinished();
		}
		else if(newState.data == 1){
			interval = setInterval(getTime, 1000);
		}
		else if(newState.data == 2){
			clearInterval(interval);
			getTime();				
		}
		else{
			clearInterval(interval);
		}
	}
	
	window.onload = function(){	
		player = new YT.Player('player', {
			height: '315',
			width: '560',
			videoId: '0N32mYbtR5k',
			playerVars: {
				wmode:'transparent',
				autohide: 1,
				rel: 0, 
				showinfo: 0    
			}
		});			
		player.addEventListener("onStateChange", "onytplayerStateChange");		
	};
	
	function getTime(){
		var time = 0;
		try {
			time = Math.round(player.getCurrentTime());
		}
		catch(e) {
			//
		}		
		
		//console.log("time: " + time + ", " + player.getCurrentTime());
		for(var i=0;i<pitstop.length;i++){
			if(pitstop[i].time == time){
				//console.log("Pausando vídeo, time: " + time + ", boxid: " + pitstop[i].boxid);
				//$(".videoBox").hide();
				//$("#" + pitstop[i].boxid).show();
				pitstop[i].callback();
				//player.pauseVideo();				
				break;
			}
		}
		
	}	
	
	$(function(){
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		  console.log(e);
		});
	});