//Função calcular porcentagem
function calcularPorcentagem() {
    var porcentagem01;
    var porcentagem02;
    var porcentagem03;

    // calculando porcentagem...
    porcentagem01 = (document.getElementById("dado02").value * 100) / document.getElementById("dado01").value;
    porcentagem02 = (document.getElementById("dado03").value * 100) / document.getElementById("dado01").value;
    porcentagem03 = (document.getElementById("dado04").value * 100) / document.getElementById("dado01").value;


    // Apresentando os valores calculados...
	document.getElementById("tabela_mapa").style.display = "block";
	document.getElementById("mdado01").value = document.getElementById("dado01").value;
    document.getElementById("mdado02").value = document.getElementById("dado02").value + " - " +(porcentagem01+"%");
    document.getElementById("mdado03").value = document.getElementById("dado03").value + " - " +(porcentagem02+"%");
    
  
}

//Função para campo somente números
function InputNum(e){
 var tecla=(window.event)?event.keyCode:e.which;
 if((tecla>47 && tecla<58)) return true;
 else{
 if (tecla==8 || tecla==0) return true;
 else  return false;
 }
}


// Funçao para tratar eventos
function adicionarEvento(objeto, Evento, funcao) {
	if(objeto.addEventListener){ // todos navegadores menos IE
		objeto.addEventListener(Evento, funcao, false); 
		return true;
	} else if (objeto.attachEvent){ // IE	
		var r = objeto.attachEvent('on'+Evento, funcao);
		return r;
	} else {
		return false;
	}
}
// mostra a linha inserida na tabela
var efeitos = {
	corFundo: function(tabela,id,cor){
		var elem = document.getElementById(tabela);
		elem.rows[id].style.background=cor;
	},
	mostra: function(tabela,id,valor){
		var tab = document.getElementById(tabela);
		var elem = tab.rows[id];
		if(elem.style.display == "none") elem.style.display = "";
		if(!valor || valor == undefined) valor=0;
		if(valor < 100){
			valor+=10;
			alpha = valor/100;
			elem.style.MozOpacity=alpha;
			elem.style.filter="alpha(opacity="+valor+")";
			elem.style.opacity = alpha;
			setTimeout("efeitos.mostra('"+tabela+"',"+id+","+valor+")",20);
		}
	},
	limpa: function(idtabela,id,valor){
		var tab = document.getElementById(idtabela);
		var totLinhas = tab.rows.length;
		if(id==undefined) id=1;
		var elem = tab.rows[id];
		this.corFundo(idtabela,id,"#428bca");
		if(elem.style.display == "none") elem.style.display = "";
		if(valor == undefined) valor=100;
		if(valor > 0){
			valor-=10;
			alpha = valor/100;
			elem.style.MozOpacity=alpha;
			elem.style.filter="alpha(opacity="+valor+")";
			elem.style.opacity = alpha;
			setTimeout("efeitos.limpa('"+idtabela+"',"+id+","+valor+")",10);
		} else {
			//elem.style.display="none";
			id++;
			if(id<totLinhas){				
				setTimeout("efeitos.limpa('"+idtabela+"',"+id+",100)",10);
			} else tabela.limpa(tab);
		}
	}
}
// Objeto para manipular a tabela
var tabela = {
	limpa: function(tabela){
		totalLinhas = tabela.rows.length;
		for(var i=1;i<totalLinhas;i++){
			//Seleciona a segunda linha para ser excluida 
			// para que não seja removido o titulo
			tabela.deleteRow(0);
		}
		var linha = tabela.insertRow(0); // Insere uma nova linha
		var coluna = linha.insertCell(0); // Insere uma coluna na linha
		coluna.setAttribute("cols", 3); // Define colspan = 3
		coluna.innerHTML="Sem dados a serem exibidos"; // Texto informativo
		inicia();
	},
	insere: function(tabela,dado1,dado2,dado3){
		// Primeiro testamos se a primeira linha nao eh a mensagem "Sem dados..."
		if(tabela.rows.length>1){
			if(tabela.rows[1].cells[0].innerHTML=="Sem dados a serem exibidos") 
				tabela.deleteRow(1); // se for apagamos
		}
			
		proxLinha = tabela.rows.length; // pega o total de linhas da tabela para acrescentar a nova
		var linha = tabela.insertRow(proxLinha); // Insere uma nova linha
		var colunaDado1 = linha.insertCell(0); // Insere a coluna Dado1
		var colunaDado2 = linha.insertCell(1); // Insere a coluna Dado2
		var colunaDado3 = linha.insertCell(2); // Insere a coluna Dado3
		var colunaCancela = linha.insertCell(3); // Insere a coluna Cancelar
		
		//Abaixo inserimos o conteudo nas colunas criadas 
		colunaDado1.innerHTML=dado1;
		colunaDado2.innerHTML=dado2;
		colunaDado3.innerHTML=dado3;
		colunaCancela.innerHTML="<a href='cancelar' rel='cancela_cliente'>limpar este registro?</a>";
		efeitos.mostra(tabela.id,linha.rowIndex);
		inicia(); // atualiza os links
	},
	apaga: function(tabela,linha){
		id = linha.rowIndex; 
				
		var confirma = confirm("Tem certeza que deseja apagar este registro?");
		if(confirma) {
			tabela.deleteRow(id);
			if(tabela.rows.length<2){
				var linha = tabela.insertRow(1); // Insere uma nova linha
				var coluna = linha.insertCell(0); // Insere uma coluna na linha
				coluna.setAttribute("cols", 3); // Define colspan = 3
				coluna.innerHTML="Sem dados a serem exibidos"; // Texto informativo		
			}			
			inicia(); // atualiza os links
		}
	}	
}
function inicia(){
	var pegaLinks = document.getElementsByTagName("a");
	for(var i=0;i<pegaLinks.length;i++){
		if(pegaLinks[i].getAttribute("rel")){
			var link_atual = pegaLinks[i].getAttribute("rel");
			if(link_atual=='cancela_cliente'){
				pegaLinks[i].onclick=function(){
					var tabela_mapa = document.getElementById("tabela_mapa");
					// linha abaixo comentada, aqui viria o Ajax para chamar a pagina que cancelaria o cliente do banco de dados
					//fsAjax("cancelar", "mensagem", "Aguarde..."); 
					
					// Voltamos para o pai do link que eh a coluna e depois para o pai
					// da coluna que é a linha.. entao pegamos sua posiçao na tabela
					//var linha_atual = this.parentNode.parentNode.rowIndex;
					tabela.apaga(tabela_mapa,this.parentNode.parentNode)
					
					return false;
				}
			}
			if(link_atual=='limpa_tabela'){
				pegaLinks[i].onclick=function(){
					var tabela_mapa = document.getElementById("tabela_mapa");
					// linha abaixo comentada, aqui viria o Ajax para chamar a pagina que limparia a tabela no  banco de dados
					//fsAjax("limpar.php", "mensagem", "Aguarde..."); 
					efeitos.limpa(tabela_mapa.id); // com efeito
					//tabela.limpa(tabela_mapa); // sem efeito
					return false;
				}
			}			
		}
	}
	// ====================
	// Pega o envio do formulario de inserir dados
	var pegaInputs = document.getElementsByTagName("input");
	for(var i=0;i<pegaInputs.length;i++){
		var input_tipo = pegaInputs[i].getAttribute("type");
		var input_nome = pegaInputs[i].getAttribute("name");
		if(input_tipo=='submit' && input_nome=='inserir'){
			pegaInputs[i].onclick=function(){
				var tabela_mapa = document.getElementById("tabela_mapa");
				
				/* ### ALTERACAO REALIZADA AQUI ### */
					//var form_insere = document.inserir;
					calcularPorcentagem();
					var form_insere = document.forms[0];
					var dado1 = form_insere.dado1.value;
					var dado2 = form_insere.mdado2.value;
					var dado3 = form_insere.mdado3.value;
					// var dado2 = form_insere.dado2.value;
					// var dado3 = form_insere.dado3.value;
				/* ### ALTERACAO REALIZADA AQUI ### */
				
				// aqui viria o Ajax para enviar o formulario e cadastrar no banco de dados
				tabela.insere(tabela_mapa,dado1,dado2,dado3);
				//fsAjax("index.asp?cadastro", "mensagem", "Aguarde...","POST");
				return false;
			}
		}
	}
}
adicionarEvento(window, "load", inicia);

// Tabela 