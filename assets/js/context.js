var quizContext = [
  {
  "question": "",
  "choices": [""],
  },
  // [1] Questão 01 - Unidade 3 - Campos e Floresta
  {
  "question": "<b>Após assistir o vídeo da pescadora Josana e de ler o trecho do documento do Ministério da Saúde, leia as assertivas e complete escolhendo A, B ou C para cada item.</b><br><br> 1 - Eu posso identificar a(s) cultura(s) a qual eu pertenço e o significado deste pertencimento, incluindo as relações das pessoas dentro do meu grupo com as pessoas de outros grupos, historicamente, institucionalmente, etc.",
  "choices": ["Frequentemente",
  "Ocasionalmente",
  "Raramente"],
  callback: function() {criarQuestao(2, "contentQuestion");}
  },
  {
  "question": "2 - Eu posso identificar as necessidades de pacientes culturalmente diferentes, em especial os pertencentes às populações do campo da floresta e das águas.",
  "choices": ["Frequentemente",
  "Ocasionalmente",
  "Raramente"],
  callback: function() {criarQuestao(3, "contentQuestion");}
  },
  {
  "question": "3 - Eu posso reconhecer minhas perspectivas familiares e culturais sobre o que parece normal ou o que são códigos de conduta inaceitáveis e como isto pode ou não variar em outras culturas e estruturas familiares.",
  "choices": ["Frequentemente",
  "Ocasionalmente",
  "Raramente"],
  callback: function() {criarQuestao(4, "contentQuestion");}
  },
  {
  "question": "4 - Eu me comunico com pacientes e colegas com base nos valores de cordialidade e assertividade.",
  "choices": ["Frequentemente",
  "Ocasionalmente",
  "Raramente"],
  callback: function() {criarQuestao(5, "contentQuestion");}
  },
  {
  "question": "5 - Eu reconheço as teorias predominantes na área da saúde e chamo a atenção de meus colegas a respeito dos aspectos destas teorias (exames, intervenções, etc) que podem diferir dos valores dos grupos socialmente vulneráveis.",
  "choices": ["Frequentemente",
  "Ocasionalmente",
  "Raramente"],
  callback: function() {criarQuestao(6, "contentQuestion");}
  },
  {
  "question": "6 - Eu estou ciente dos aspectos legais que protegem as pessoas, comunidades e populações quanto à discriminação, inclusive na área da saúde.",
  "choices": ["Frequentemente",
  "Ocasionalmente",
  "Raramente"],
  callback: function() {criarQuestao(7, "contentQuestion");}
  },
  {
  "question": "7 - Eu busco Prestar serviços respeitando as diversas culturas e saberes apresentados pelo (a) paciente, família e ou comunidade.",
  "choices": ["Frequentemente",
  "Ocasionalmente",
  "Raramente"],
  callback: function() {criarQuestao(8, "contentQuestion");}
  },
  {
  "question": "8 - No serviço de saúde onde trabalho eu defendo ativamente o tratamento igualitário e respeitoso entre os profissionais e para os clientes sem privilégios de qualquer natureza.",
  "choices": ["Frequentemente",
  "Ocasionalmente",
  "Raramente"],
  callback: function() {result();}
  },
];

var ID_QUESTION = 0;

function erase() {
  $("#contentQuestion").hide();
}

var a = 0;
var b = 0;
var c = 0;
var score = [a, b, c];

function criarQuestao(questionIndex, comp){
  var question = quizContext[questionIndex];
  var content = $("#" + comp);
  content.show();
  var choices = question.choices;
  var indexChar = 97;

  var str = "<form id='quizContext_box_id_" + ID_QUESTION + "'><p class='atividade-titulo'>" + question.question + "</p>";
  for(var i=0;i<choices.length;i++){
    str += "<label class='choiceStyle' style='font-weight:100 !important;'><input type='radio' name='choice' value='" + (i + 1) + "'/> " + String.fromCharCode(indexChar++) + ") " + choices[i] + "</label><br/>";
  }
  str += "<input type='button' class='btn btn-sm btn-warning' value='Enviar'/></form>";

  content.html(str);
  $("#quizContext_box_id_" + ID_QUESTION + " input[type=button]").click({index: questionIndex}, function(e){
    var box = $(this).parent();
    var item = $(box).find("input[type=radio]:checked");
    var questionItem = $(item).val();
    if(questionItem == undefined) {
      return;
    } else if(questionItem == 1) {
      a++;
      score = [a, b, c];
    } else if(questionItem == 2) {
      b++;
      score = [a, b, c];
    } else if(questionItem == 3) {
      c++;
      score = [a, b, c];
    }
    question.callback();
  });
  ID_QUESTION++;
}

function max_index(elements) {
    var i = 1;
    var mi = 0;
    while (i < elements.length) {
        if (!(elements[i] < elements[mi]))
            mi = i;
        i += 1;
    }
    return mi;
}

function result() {
  var feedback;
  if (max_index(score) == 0) {
    feedback = "A maioria das suas respostas foi <b>“A”</b>: é sugestivo que você demonstre conhecimento sobre  culturas diferentes, comunicando-se terapeuticamente com seus pacientes e não com possíveis estereótipos. Mostra ainda forte compromisso em se engajar na educação permanente sobre saúde das populações do campo, da floresta e das águas.<p><b>RESULTADO:</b></p>A: "+score[0]+" questões, B: "+score[1]+" questões, C: "+score[2]+" questões";
  } else if (max_index(score) == 1) {
    feedback = "A maioria das suas respostas foi <b>“B”</b>: é sugestivo que a eficácia do encontro clínico com seus pacientes possa estar prejudicada pela existência de pré-conceitos culturais e estereótipos. Os estereótipos limitam a habilidade de qualquer profissional de saúde para se engajar na educação permanente sobre saúde de populações vulneráveis e  culturalmente diferentes.<p><b>RESULTADO:</b></p>A: "+score[0]+" questões, B: "+score[1]+" questões, C: "+score[2]+" questões";
  } else if (max_index(score) == 2) {
    feedback = "A maioria das suas respostas foi <b>“C”</b>: é sugestivo que haja falta de conhecimento sobre a diversidade cultural e sobre os direitos humanos essenciais. Seria importante uma reflexão que ajudasse você a perceber a necessidade de se engajar nos processos de educação na saúde e na permanente a partir do respeito às diversas culturas. <p><b>RESULTADO:</b></p>A: "+score[0]+" questões, B: "+score[1]+" questões, C: "+score[2]+" questões";
  }

  var customModal = $('<div class="custom-modal modal fade"><div class="modal-dialog"><div class="modal-content modal-lg-nuteds">' +
    '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
    '<h4 class="modal-title">Feedback</h4></div>' +
    '<div class="modal-body"><p>Obrigada por responder este pré-teste. Nele não há respostas certas ou erradas. Ele visa exclusivamente estimular sua autoconsciência sobre a diversidade cultural das populações do campo, floresta e das águas e a importância disso na prática do cuidado de saúde.</p><p>' + feedback + '</p><p>Adaptado por Isabel Cruz de: US Department of Health and Human Services. Culturally Competent Nursing Modules. 2007. Disponível em <a href="https://ccnm.thinkculturalhealth.hhs.gov/" target="_blank">LINK</a></p></div>' +
    '<div class="modal-footer">' +
    '<button type="button" class="btn btn-warning" data-dismiss="modal">Ok</button>' +
    '</div></div></div></div>');

    $('body').append(customModal);
    $('.custom-modal').modal();

    $('.custom-modal').on('hidden.bs.modal', function(){
        console.log("teste");
        // window.location.href = addressLink;
        $('.custom-modal').remove();
    });
}
