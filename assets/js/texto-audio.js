var recurso = [{
    "audio": "slide_1.mp3",
    "texto": "Destacamos na tabela acima os estados de São Paulo, Minas Gerais, Rio de Janeiro, Bahia e Amazonas pela quantidade de conflitos que envolvem injustiça ambiental no Brasil.  O elevado número de conflitos nos estados do Sudeste está relacionado ao histórico de intensa ocupação territorial e de industrialização com inúmeros impactos socioambientais, bem como aos movimentos sociais organizados na região. <br><br> Mas é em regiões como o Nordeste, Norte e Centro-Oeste que atualmente se encontra a fronteira de expansão capitalista no país, principalmente por conta do agronegócio, do ciclo da mineração e de inúmeras obras de infraestrutura, como hidrelétricas, rodovias e  a transposição do São Francisco. As regiões Norte e Nordeste, juntas, representam cerca de 50% dos casos de conflitos apresentados.<br><br> Frequentemente, tais casos envolvem vastos territórios e diversos municípios simultaneamente, dada a extensão dos conflitos decorrentes. –Em estados como o Amapá, os conflitos envolvem 100% dos municípios, no Acre 64%e no Mato Grosso 61%, enquanto em São Paulo esse percentual é menor que 6%, embora seja o estado com maior número de conflitos. O Rio de Janeiro é uma exceção no Sudeste nesse aspecto, pois os conflitos atingem 63% dos municípios, fato explicado pela existência de casos relacionados a grandes complexos industriais e portuários, além de desastres químicos no rio Paraíba do Sul, que passa por inúmeros municípios."
}, {
    "audio": "slide_2.mp3",
    "texto": "As principais populações atingidas pelos conflitos são as que vivem nos campos, em florestas e na região costeira nos territórios da expansão capitalista: povos indígenas, agricultores familiares, comunidades quilombolas, pescadores artesanais e ribeirinhos. Mas também se destacam populações urbanas dentre os atingidos, como moradores de áreas próximas a lixões, operários e moradores de bairros atingidos por acidentes ambientais."
}, {
    "audio": "slide_3.mp3",
    "texto": "Os principais impactos socioambientais referem-se à alteração no regime tradicional do uso de solo, bem como a problemas na demarcação dos territórios de terras indígenas, quilombolas ou para a reforma agrária. <br><br> Tais impactos estão relacionados à disputa por territórios por parte de setores econômicos, como o agronegócio, a mineração ou a obras de infraestrutura. Outros impactos de grande importância são a poluição (hídrica, do solo e atmosférica), o desmatamento, problemas no licenciamento ambiental, alteração no ciclo reprodutivo da fauna, invasão ou danos a áreas de proteção ambiental, assoreamento dos rios e a erosão do solo. <br><br> A questão do licenciamento ambiental é de particular importância, pois encontra-se presente em praticamente todos os casos nos quais o que está em jogo é um novo empreendimento econômico, sejam hidrelétricas, siderúrgicas, aterros sanitários, indústrias petroquímicas ou “ecoresorts” turísticos. <br><br> Via de regra, as denúncias apontam a falta de participação e de critérios técnicos vinculados à legislação ambiental e sanitária existente. Embora de menor importância no Mapa, o impacto nos territórios urbanos está presente em questões como poluição, enchentes, formação de lixões, acidentes ambientais e regulação fundiária."
}, {
    "audio": "slide_4.mp3",
    "texto": "O Mapa apresenta uma concepção ampliada de saúde, que reflete não somente a dimensão biomédica dos impactos ambientais, como também questões relacionadas à qualidade de vida, à cultura e tradições, aos direitos humanos e à capacidade de organização e mobilização coletivas. <br><br> O resultado indica a piora na qualidade de vida como o principal problema de saúde levantado pelas populações atingidas em suas lutas, e isso decorre da percepção de como a disputa territorial e o modelo de desenvolvimento estão impactando ou poderão vir a impactar seus modos de vida. <br><br>  O que está em jogo, nesse caso, não é apenas evitar os prejuízos decorrentes de certos impactos ambientais, como a poluição, mas a manutenção de certos valores, práticas sociais e relações com a natureza, que foram ou serão perdidos diante do “progresso” econômico, no aproveitamento de recursos naturais e na disputa por território. <br><br> Portanto, o conceito de qualidade de vida representa uma visão complexa, que rejeita a ideia de crescimento, riqueza e consumismo à custa da perda dos próprios valores e sentidos de vida comunitária, em especial nos povos das florestas, campos e regiões, onde os ecossistemas se encontram mais preservados e a subsistência depende de sua vitalidade.  <br><br> Outra questão de grande importância é a presença da violência como problema de saúde em suas várias formas: desde a coação e ameaça até os assassinatos. Ela expressa a questão fundamental dos direitos humanos em nosso país, ou seja, de como as populações atingidas e vulnerabilizadas sofrem com a falta de cidadania, principalmente pela impossibilidade de exercer o direito à organização coletiva para reivindicarem e protestarem contra as injustiças que lhes são acometidas.  <br><br> Outras questões básicas de saúde referem-se ao problema de insegurança alimentar, das doenças não transmissíveis (como o câncer e as doenças respiratórias decorrentes da poluição química), dos acidentes e, atrelados a todos estes problemas, da falta de assistência médica adequada e de estudos que associem tais problemas de saúde com os problemas ambientais na região.  <br><br> Também o agravamento das doenças transmissíveis pela degradação ambiental e falta de saneamento básico aparecem de forma relevante nos conflitos."
}, {
    "audio": "slide_5.mp3",
    "texto": "Existem dois grandes grupos de causas de injustiças ambientais que aparecem reunidos neste item. O primeiro refere-se às atividades econômicas e seus agentes que, ao interferirem nos territórios e modos de vida das populações, geram inúmeros impactos e conflitos. <br><br> Tais atividades expressam os principais eixos econômicos que orientam o atual modelo de desenvolvimento brasileiro em sua inserção na economia capitalista globalizada. Dentre eles destacam-se, nessa ordem, o agronegócio, a mineração e a siderurgia, a construção de barragens e hidrelétricas, as madeireiras, as indústrias químicas e petroquímicas, as atividades pesqueiras e a carcinicultura, a pecuária e a construção de rodovias, hidrovias e gasodutos. <br><br> Na categoria “outros” aparecem de forma destacada os setores turístico e imobiliário na disputa territorial, que sistematicamente buscam expulsar populações dos locais onde vivem, sejam as tradicionais, dos “paraísos ecológicos” dos “eco RESORTS”, sejam as que vivem em áreas urbanas, onde os moradores pobres e de favelas são frequentemente acusados de serem os responsáveis pela degradação ambiental e pela violência nas cidades. <br><br> O segundo grupo responsável por injustiças ambientais está associado à atuação, ou melhor, à deficiência do próprio poder público e de entidades governamentais, incluindo problemas associados à atuação do judiciário e/ou dos ministérios públicos e à deficiência das políticas públicas e legislação ambiental. <br><br> Destacam-se aqui problemas relacionados à forma como os licenciamentos ambientais são realizados, bem como à morosidade ou deficiência das instituições da justiça em defenderem os interesses coletivos das populações atingidas."
}];

function recursoAudio(numAudio) {

    var numAudio = numAudio;

    var textoModal = recurso[numAudio].texto;
    var audioSrc = recurso[numAudio].audio;

    var audio = $('<audio controls id="audio"><source src="audio/' + audioSrc + '" type="audio/mpeg">Seu navegador não possui suporte para esse tipo de audio. Nos informe sobre esse erro: contato@nuteds.ufc.br</audio><br><p class="text-center"><small>Para ler o texto do áudio clique no botão abaixo</small></p><a class="btn btn-warning transcricao"><span class="glyphicon glyphicon-comment"></span></a>');

    $('.audio').html(audio);
    setTimeout(function(){ 
        document.getElementsByTagName("audio")[numAudio].play();
        console.log("nos");
      }, 100);

    // Quando clicar no botão de texto
    $('.transcricao').click(function() {
        var customModal = $('<div class="custom-modal modal fade"><div class="modal-dialog"><div class="modal-content">' +
        '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '<h4 class="modal-title">Texto do Audio</h4></div>' +
        '<div class="modal-body text-justify"><p>' + textoModal + '</p></div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-warning" data-dismiss="modal">Ok</button>' +
        '</div></div></div></div>');

        $('body').append(customModal);
        $('.custom-modal').modal();
      
        $('.custom-modal').on('hidden.bs.modal', function(){
            console.log("teste");
                // window.location.href = addressLink;
            $('.custom-modal').remove();
        });
    });
}
