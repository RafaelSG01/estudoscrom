/* Plugin - Nuteds - Barra de progresso
* http://www.nuteds.ufc.br
* NUTEDS - UFC @ Copyright 2014 - Todos os direitos reservados
*/  

function progressBar() {
  var content = $(".barra");
  var pages = $(".barra-de-progresso ul li");
  var numPag = $(".barra-de-progresso ul li").length;
  var pageAtual;
  for(i=0; i < numPag; i++) {
    var indice = $(pages[i]).hasClass("pag-ativa");
    if(indice) {
      pageAtual = i;
    }
  }
  var numPag2 = numPag - 1;
  var valor = Math.round(((pageAtual * 100) / numPag2));
  var texto = '<div class="progress"><div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="'+ valor +'" aria-valuemin="0" aria-valuemax="100" style="width: '+ valor +'%">'+ valor +'%</div></div>';
  content.html(texto);
  var calc1 = (100 - (4.75 * numPag));
  var margin = calc1 / (numPag-1);
  $(".nuteds-marcador li:last-child").siblings().css({"margin-right":margin + '%'});
}

