/**
 * Recurso feito exclusivo para o infográfico interativo sobre a Dengue.
 * @author Equipe de Desenvolvimento
 * @version 1.0
 * @copyright UFC/UNASUS (Nuteds)
 */

var infografico = [
	{
		"controle": "<img src='assets/images/unidade_2/atv1_circle1.png' width='111' height='111' usemap='#Map' hidefocus='true'><map name='Map'><area shape='poly' coords='2,49,24,53,26,40,33,32,40,29,35,6,24,10,15,17,10,25,7,32' href='javascript:void(0);' class='image-map' data='0'><area shape='poly' coords='50,24,64,25,78,33,94,19,79,8,73,5,67,2,51,2,45,2' href='javascript:void(0);' class='image-map' data='1'><area shape='poly' coords='0,59,24,60,25,72,31,76,39,83,37,96,32,105,13,92,5,77' href='javascript:void(0);' class='image-map' data='4'><area shape='poly' coords='85,38,102,27,108,40,108,48,107,63,104,69,103,79,102,84,87,73' href='javascript:void(0);' class='image-map' data='2'><area shape='poly' coords='50,87,64,85,79,80,93,91,87,99,67,105,47,105' href='javascript:void(0);' class='image-map' data='3'></map>",
		"imagem": "<img src='assets/images/unidade_2/atv1_img01.svg' width='135' height='368' class='dengue-imagem-format'>",
		"conteudo": "<h4 class='text-center'><b>DENGUE</b></h4>Após a picada, o vírus atinge a corrente sanguínea instalando-se em órgãos como baço, fígado e o tecido linfático para se multiplicar."
	},
	{
		"controle": "<img src='assets/images/unidade_2/atv1_circle2.png' width='111' height='111' usemap='#Map' hidefocus='true'><map name='Map'><area shape='poly' coords='2,49,24,53,26,40,33,32,40,29,35,6,24,10,15,17,10,25,7,32' href='javascript:void(0);' class='image-map' data='0'><area shape='poly' coords='50,24,64,25,78,33,94,19,79,8,73,5,67,2,51,2,45,2' href='javascript:void(0);' class='image-map' data='1'><area shape='poly' coords='0,59,24,60,25,72,31,76,39,83,37,96,32,105,13,92,5,77' href='javascript:void(0);' class='image-map' data='4'><area shape='poly' coords='85,38,102,27,108,40,108,48,107,63,104,69,103,79,102,84,87,73' href='javascript:void(0);' class='image-map' data='2'><area shape='poly' coords='50,87,64,85,79,80,93,91,87,99,67,105,47,105' href='javascript:void(0);' class='image-map' data='3'></map>",
		"imagem": "<img src='assets/images/unidade_2/atv1_img02.svg' width='135' height='368' class='dengue-imagem-format'>",
		"conteudo": "<h4 class='text-center'><b>DENGUE</b></h4>Após o período de incubação, que varia de 2 a 7 dias, o vírus volta a corrente sanguínea replicando-se e atingindo a medula óssea diminuindo a produção de plaquetas elemento fundamental na coagulação do sangue. "
	},
	{
		"controle": "<img src='assets/images/unidade_2/atv1_circle3.png' width='111' height='111' usemap='#Map' hidefocus='true'><map name='Map'><area shape='poly' coords='2,49,24,53,26,40,33,32,40,29,35,6,24,10,15,17,10,25,7,32' href='javascript:void(0);' class='image-map' data='0'><area shape='poly' coords='50,24,64,25,78,33,94,19,79,8,73,5,67,2,51,2,45,2' href='javascript:void(0);' class='image-map' data='1'><area shape='poly' coords='0,59,24,60,25,72,31,76,39,83,37,96,32,105,13,92,5,77' href='javascript:void(0);' class='image-map' data='4'><area shape='poly' coords='85,38,102,27,108,40,108,48,107,63,104,69,103,79,102,84,87,73' href='javascript:void(0);' class='image-map' data='2'><area shape='poly' coords='50,87,64,85,79,80,93,91,87,99,67,105,47,105' href='javascript:void(0);' class='image-map' data='3'></map>",
		"imagem": "<img src='assets/images/unidade_2/atv1_img03.svg' width='135' height='368' class='dengue-imagem-format'>",
		"conteudo": "<h4 class='text-center'><b>DENGUE</b></h4>Durante esse processo formam-se substâncias que inflamam as paredes dos vasos sanguíneos, tornando-os mais permeáveis, podendo provocar o extravasamento do fluxo sanguíneo."
	},
	{
		"controle": "<img src='assets/images/unidade_2/atv1_circle4.png' width='111' height='111' usemap='#Map' hidefocus='true'><map name='Map'><area shape='poly' coords='2,49,24,53,26,40,33,32,40,29,35,6,24,10,15,17,10,25,7,32' href='javascript:void(0);' class='image-map' data='0'><area shape='poly' coords='50,24,64,25,78,33,94,19,79,8,73,5,67,2,51,2,45,2' href='javascript:void(0);' class='image-map' data='1'><area shape='poly' coords='0,59,24,60,25,72,31,76,39,83,37,96,32,105,13,92,5,77' href='javascript:void(0);' class='image-map' data='4'><area shape='poly' coords='85,38,102,27,108,40,108,48,107,63,104,69,103,79,102,84,87,73' href='javascript:void(0);' class='image-map' data='2'><area shape='poly' coords='50,87,64,85,79,80,93,91,87,99,67,105,47,105' href='javascript:void(0);' class='image-map' data='3'></map>",
		"imagem": "<img src='assets/images/unidade_2/atv1_img04.svg' width='135' height='368' class='dengue-imagem-format'>",
		"conteudo": "<h4 class='text-center'><b>DENGUE</b></h4>Na dengue clássica esse sangramento é sútil."
	},
	{
		"controle": "<img src='assets/images/unidade_2/atv1_circle5.png' width='111' height='111' usemap='#Map' hidefocus='true'><map name='Map'><area shape='poly' coords='2,49,24,53,26,40,33,32,40,29,35,6,24,10,15,17,10,25,7,32' href='javascript:void(0);' class='image-map' data='0'><area shape='poly' coords='50,24,64,25,78,33,94,19,79,8,73,5,67,2,51,2,45,2' href='javascript:void(0);' class='image-map' data='1'><area shape='poly' coords='0,59,24,60,25,72,31,76,39,83,37,96,32,105,13,92,5,77' href='javascript:void(0);' class='image-map' data='4'><area shape='poly' coords='85,38,102,27,108,40,108,48,107,63,104,69,103,79,102,84,87,73' href='javascript:void(0);' class='image-map' data='2'><area shape='poly' coords='50,87,64,85,79,80,93,91,87,99,67,105,47,105' href='javascript:void(0);' class='image-map' data='3'></map>",
		"imagem": "<img src='assets/images/unidade_2/atv1_img05.svg' width='135' height='368' class='dengue-imagem-format'>",
		"conteudo": "<h4 class='text-center'><b>DENGUE</b></h4>Já na Dengue hemorrágica essas manifestações aparecem ou pioram provocando hemorragias espontâneas nos pulmões, aparelho digestivo, cavidade torácica e pele, podendo até mesmo atingir o cérebro. Além disso, com a diminuição de plaquetas e a perda de sangue e plasma, haverá queda de pressão arterial, podendo ocorrer o choque."
	}
];

function desenharInfografico(indice) {
	$("#infografico-dengue").show;
	var indice = indice;
	var controle = $(".dengue-controle");
	var conteudo = $(".dengue-texto");
	var imagem = $(".dengue-imagem");
	controle.html(infografico[indice].controle);
	conteudo.html(infografico[indice].conteudo);
	imagem.html(infografico[indice].imagem);
	$('.image-map').click(function() {
	    var botao = $(this).attr('data');
	    desenharInfografico(botao);
	});
}