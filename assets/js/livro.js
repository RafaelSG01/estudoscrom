function abrirLivro() {
      var livro = $(".component").html();
      var customModal = $('<div class="custom-modal modal fade"><div class="modal-dialog"><div class="modal-content">' +
    '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
    '<h4 class="modal-title">Livro</h4></div>' +
    '<div class="modal-body" style="background:#ecf0f1">'+ livro +'</div>' +
    '<div class="modal-footer">' +
    '<button type="button" class="btn btn-warning" data-dismiss="modal">Ok</button>' +
    '</div></div></div></div>');

    $('body').append(customModal);
    $('.custom-modal').modal();
  
    $('.custom-modal').on('hidden.bs.modal', function(){
        console.log("teste");
        $('.custom-modal').remove();
    });
    // Animação
      function animation() {
        $(".hardcover_front").css({
          "transform": "rotateY(-145deg) translateZ(0)",
          "-webkit-transform": "rotateY(-145deg) translateZ(0)",
          "z-index": "0"
        });
        $(".page li:nth-child(5)").css({
          "transform": "rotateY(-140deg)",
          "-webkit-transform": "rotateY(-140deg)",
          "transition-duration": "1.2s",
          "-webkit-transition-duration": "1.2s"
        });
        $(".page li:nth-child(4)").css({
          "transform": "rotateY(-130deg)",
          "-webkit-transform": "rotateY(-130deg)",
          "transition-duration": "1.4s",
          "-webkit-transition-duration": "1.4s"
        });
        $(".page li:nth-child(3)").css({
          "transform": "rotateY(-118deg)",
          "-webkit-transform": "rotateY(-118deg)",
          "transition-duration": "1.6s",
          "-webkit-transition-duration": "1.6s"
        });
        $(".page li:nth-child(2)").css({
          "transform": "rotateY(-35deg)",
          "-webkit-transform": "rotateY(-35deg)",
          "transition-duration": "1.8s",
          "-webkit-transition-duration": "1.8s"
        });
        $(".page li:nth-child(1)").css({
          "transform": "rotateY(-30deg)",
          "-webkit-transform": "rotateY(-30deg)",
          "transition-duration": "1.5s",
          "-webkit-transition-duration": "1.5s"
        });
      }
      setTimeout(animation,1300);
    }