// Setando namespace/módulo global
var aguas = {};

// Módulo do jogo
aguas.Game = (function() {

  // Inicialização de todos os componentes do jogo
  function Field() {
    var objects = 3;
    var canvas = document.getElementById("canvas");
    var stage  = new createjs.Stage(canvas);
    createjs.Touch.enable(stage);

    // Conteúdo
    var content = [{
      "id": 0,
      "question": "Em áreas florestais, as peculiaridades exercem grande relevância para o adoecimento da população. Dentre essas peculiaridades, algumas envolvem as parasitoses intestinais devido a falta de saneamento básico, infraestrutura e educação sanitária.  Em casos de alta incidência de parasitoses intestinais, os profissionais de saúde deverão orientar a população a:",
      "options": ["Evitar o contato direto com o solo contaminado por resíduos sólidos e fezes, a  utilização de água proveniente de fontes não tratadas.", "Tomar banho em lagoa, cisternas, córregos e rios com água parada não exerce interferência no contágio pelas parasitoses intestinais.", "Evitar ingerir alimentos crus e não lavados, evitar tomar banho de chuva e tomar água filtrada ou fervida."],
      "feedback": ["Parabéns! Esta é a melhor alternativa de resposta à questão.  Solo contaminado e água não tratada podem transmitir infecções intestinais que podem entrar no corpo via ingestão e até através da pele.", "Esta não é a melhor alternativa de resposta à questão. Tomar banho em lagoa, cisternas, córregos e rios com água parada pode ser perigoso uma vez que infecções intestinais podem entrar no corpo através da pele e a pessoa pode ingerir a água sem perceber.", "Esta não é a melhor alternativa de resposta à questão. Tomar banho de chuva não é um perigo para contrair infecções intestinais desde que a pessoa não esteja em contato direto com o solo."],
      "correct": 1,
      callback: function() {
        objects--;
        stage.removeChild(tipOne);
        success = new createjs.Bitmap(aguas.graphics.success.path);
        success.x = 245;
        success.y = 434;
        stage.addChild(success);
        updateStage();
      },
      callbackWrong: function() {
        endGame();
        stage.removeChild(tipOne);
        error = new createjs.Bitmap(aguas.graphics.error.path);
        error.x = 245;
        error.y = 434;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }, {
      "id": 1,
      "question": "Sobre a Saúde da Mulher na área rural comparada a área urbana, pode-se afirmar que:",
      "options": ["Há maior dificuldade por parte das gestantes residentes em áreas urbanas em realizar o pré-natal, inclusive em ter o acesso ao cartão da gestante.",
      "As mulheres da área rural são melhores assistidas, tendo maior acesso aos serviços de saúde por formarem uma população menor que a urbana.",
      "Há uma dificuldade em direcionar a assistência à trabalhadora rural, dando maior enfoque a especifidade da relação trabalho no campo e saúde."],     
      "feedback": ["Esta não é a melhor alternativa de resposta à questão. 30% das mulheres da área rural e 8% das mulheres da área urbana não realizaram nenhuma consulta de pré-natal. Das mulheres residentes na área rural que receberam atendimento pré-natal, apenas 36% tiveram acesso ao cartão da gestante.", "Esta não é a melhor alternativa de resposta à questão. A dificuldade das mulheres rurais no acesso às informações e ações de saúde estão relacionadas, dentre outros fatores, às desigualdades das relações de gênero e de trabalho, às grandes distâncias entre a residência ou trabalho e os serviços de saúde e à maior precariedade dos serviços locais.", "Parabéns! Esta é a melhor alternativa de resposta à questão. Existem poucos estudos enfocando os problemas de saúde da mulher residente e trabalhadora rural, o que dificulta a proposição de ações adequadas a essa realidade. Em assentamentos e acampamentos rurais, essa carência é ainda maior."],
      "correct": 3,
      callback: function() {
        objects--;
        stage.removeChild(tipTwo);
        success = new createjs.Bitmap(aguas.graphics.success.path);
        success.x = 698;
        success.y = 450;
        stage.addChild(success);
        updateStage();
        endGame();
      },
      callbackWrong: function() {
        stage.removeChild(tipTwo);
        error = new createjs.Bitmap(aguas.graphics.error.path);
        error.x = 698;
        error.y = 450;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }, {
      "id": 2,
      "question": "A Política Nacional de Saúde Integral das Populações do Campo, da Floresta e das Águas  (PNSIPCFA)  foi publicada em dezembro de 2011 e representa o compromisso firmado entre o Estado brasileiro com a saúde dessas populações. As populações beneficiadas diretamente pela PNSIPCF são aquelas definidas na Portaria nº 2.866, de 2 de dezembro de 2011 como sendo  povos e comunidades que têm seus modos de vida, produção e reprodução social relacionados, predominantemente, com a terra. Neste contexto estão os camponeses, sejam eles agricultores familiares, trabalhadores rurais assentados ou acampados, assalariados e temporários que residam ou não no campo. Além dessas populações, são beneficiadas com a Política Nacional de Saúde das Populações do Campo, da Floresta e das Águas:",
      "options": ["As comunidades tradicionais, como as ribeirinhas, quilombolas e as que habitam ou usam reservas extrativistas em áreas florestais ou aquáticas e ainda as populações atingidas por barragens.",
      "As comunidades camponesas, os agronegócios, os trabalhadores rurais assentados, quilombolas, as que habitam ou usam reservas extrativistas, populações ribeirinhas, populações atingidas por barragens.",
      "As comunidades tradicionais, as ribeirinhas, as quilombolas e as que habitam ou usam reservas extrativistas em áreas florestais ou urbanas  e ainda as populações que habitam as barragens."],
      "feedback": ["Parabéns! Esta é a melhor alternativa de resposta à questão.  Essas comunidades são beneficiadas com a Política Nacional de Saúde das Populações do Campo, da Floresta e das Águas pois são comunidades que têm seus modos de vida, produção e reprodução social relacionados, predominantemente, com a terra.", "Esta não é a melhor alternativa de resposta à questão. Os agronegócios não são beneficiadas com a Política Nacional de Saúde das Populações do Campo, da Floresta e das Águas.", "Esta não é a melhor alternativa de resposta à questão. As comunidades que habitam ou usam reservas extrativistas em áreas urbanas não se enquadram nas características de populações a ser beneficiadas com a Política Nacional de Saúde das Populações do Campo, da Floresta e das Águas."],
      "correct": 1,
      callback: function() {
        objects--;
        stage.removeChild(tipThree);
        success = new createjs.Bitmap(aguas.graphics.success.path);
        success.x = 452;
        success.y = 412;
        stage.addChild(success);
        updateStage();
        endGame();
      },
      callbackWrong: function() {
        stage.removeChild(tipThree);
        error = new createjs.Bitmap(aguas.graphics.error.path);
        error.x = 452;
        error.y = 412;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }];

    // Preloader
    function loadGraphics(e) {
      var imagesList = [
        {name:"tip", path:"assets/images/game/tip.png"},
        {name:"landscape", path:"assets/images/game/agua.png"},
        {name:"success", path:"assets/images/game/success.png"},
        {name:"error", path:"assets/images/game/error.png"},
        {name:"end", path:"assets/images/game/end_agua.png"},
      ]

      aguas.graphics = {};

      var totalFiles = imagesList.length;
      var loadedFiles = 0;
      for (var i=0, len=totalFiles; i<len; i++) {
        imageToLoad = imagesList[i];
        var img = new Image();
        img.onload = (function(event) {
          loadedFiles++;
          if (loadedFiles >= totalFiles) {
            initGame();
          }
        });
        img.src = imageToLoad.path;
        aguas.graphics[imageToLoad.name] = imageToLoad;
      };
    }

    // Função auxiliar para recarregar o palco
    var updateStage = function() {
      stage.update();
    }

    // Fim do jogo
    var endGame = function() {
      if (objects == 0) {
        stage.removeAllChildren();
        updateStage();
        var end = new createjs.Bitmap(aguas.graphics.end.path);
        stage.addChild(end);
        updateStage();
      }
    }

    var drawingTip = function() {
      // Rev Verde (1)
        tipOne = new createjs.Bitmap(aguas.graphics.tip.path);
        tipOne.x = 245;
        tipOne.y = 434;
        stage.addChild(tipOne);
        tipOne.addEventListener("click", function() {
          modal(content[0].id, content[0].feedback, content[0].question, content[0].options);
        });
      // Leito do rio
        tipTwo = new createjs.Bitmap(aguas.graphics.tip.path);
        tipTwo.x = 698;
        tipTwo.y = 450;
        stage.addChild(tipTwo);
        tipTwo.addEventListener("click", function() {
          modal(content[1].id, content[1].feedback, content[1].question, content[1].options);
        });
      // Rio
        tipThree = new createjs.Bitmap(aguas.graphics.tip.path);
        tipThree.x = 452;
        tipThree.y = 412;
        stage.addChild(tipThree);
        tipThree.addEventListener("click", function() {
          modal(content[2].id, content[2].feedback, content[2].question, content[2].options);
        });
    }

    var modal = function(id, feedback, question, options, correct) {
      this.id = id;
      this.question = question;
      this.options = options;
      this.feedback = feedback;
      var indexChar = 97;
      var str = "<form id='game_form_" + this.id + "'>";
      for(var i=0;i<this.options.length;i++){
        str += "<label class='choiceStyle' style='font-weight:100 !important;'><input type='radio' name='choice' value='" + (i + 1) + "'/> " + String.fromCharCode(indexChar++) + ") " + this.options[i] + "</label><br/>";
      }
      str += "<br><input type='button' class='btn btn-sm btn-warning' value='Confirmar escolha'/></form>";
      var customModal = $('<div class="custom-modal modal fade"><div class="modal-dialog"><div class="modal-content">' +
      '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
      '<h4 class="modal-title">Questão</h4></div>' +
      '<div class="modal-body"><div class="aguas-feedback"></div><p>'+ this.question +'</p>' +
      str +
      '</div>' +
      '</div></div></div>');

      $('body').append(customModal);
      $('.custom-modal').modal();

      $('.custom-modal').on('hidden.bs.modal', function(){
        $('.custom-modal').remove();
      });
      // Checando
      $("#game_form_"+ this.id +" input[type=button]").click({index: id}, function(e){
        var box = $(this).parent();
        var item = $(box).find("input[type=radio]:checked");
        var questionItem = $(item).val();
        var itemCorrect = content[e.data.index].correct;
        if(questionItem == undefined)
          return;

        if(questionItem == itemCorrect){
          $(".aguas-feedback").html('<div class="alert alert-success text-justify game-tip" role="alert">'+ feedback[questionItem-1] +'</div>');
          $('.custom-modal').on('hidden.bs.modal', function(){
            $('.custom-modal').remove();
            content[e.data.index].callback();
          });
        } else {
          $(".aguas-feedback").html('<div class="alert alert-danger text-justify game-tip" role="alert">'+ feedback[questionItem-1] +'</div>');
          $('.custom-modal').on('hidden.bs.modal', function(){
            $('.custom-modal').remove();
            content[e.data.index].callbackWrong();
          });
        }
        $(box).find('input').attr('disabled', true);
      });
    }

    // Fase inicial
    var initGame = function() {
      // Cenário
      var landscape = new createjs.Bitmap(aguas.graphics.landscape.path);
      landscape.y = 0;
      stage.addChild(landscape);
      drawingTip();
      // Atualizar palco para carregar os elementos na tela.
      updateStage();
    }
    loadGraphics();
  }
  return Field;
})();

// Chamando o jogo
$(function() {
  var game = new aguas.Game();
});