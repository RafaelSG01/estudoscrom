function drawChart() {  
                var tmpDado01;
                var tmpDado02;
                var tmpDado03;
                var tmpDado04;
                var tmpDado05;
                var tmpDado06;
                var tmpDado07;
                var tmpDado08;
                var tmpDado09;
                var tmpPorcentagem01;
                var tmpPorcentagem02;
                var tmpPorcentagem03;
                var tmpPorcentagem04;
                var tmpPorcentagem05;
                var tmpPorcentagem06;
                var tmpPorcentagem07;
                var tmpPorcentagem08;
                var tmpPorcentagem09;

                if (document.forms[0].dado01) {
                    tmpDado01 = document.getElementById('dado01').value;
                    tmpPorcentagem01 = (document.getElementById("dado01").value * 100) / document.getElementById("dado01").value;
                } else {
                    tmpDado01 = "0";
                    tmpPorcentagem01 = "0";
                }
                
                if (document.forms[0].dado02) {
                    tmpDado02 = document.getElementById('dado02').value;
                    tmpPorcentagem02 = (document.getElementById("dado02").value * 100) / document.getElementById("dado01").value;
                    
                } else {
                    tmpDado02 = "0";
                    tmpPorcentagem02 = "0";
                }


                if (document.forms[0].dado03) {
                    tmpDado03 = document.getElementById('dado03').value;
                    tmpPorcentagem03 = (document.getElementById("dado03").value * 100) / document.getElementById("dado01").value;
                    
                    
                } else {
                    tmpDado03 = "0";
                    tmpPorcentagem03 = "0";
                }

                if (document.forms[0].dado04) {
                    tmpDado04 = document.getElementById('dado04').value;
                    tmpPorcentagem04 = (document.getElementById("dado04").value * 100) / document.getElementById("dado01").value;
                } else {
                    tmpDado04 = "0";
                    tmpPorcentagem04 = "0";
                }
                
                 if (document.forms[0].dado05) {
                    tmpDado05 = document.getElementById('dado05').value;
                    tmpPorcentagem05 = (document.getElementById("dado05").value * 100) / document.getElementById("dado01").value;
                } else {
                    tmpDado05 = "0";
                    tmpPorcentagem05 = "0";
                } 
                
                 if (document.forms[0].dado06) {
                    tmpDado06 = document.getElementById('dado06').value;
                    tmpPorcentagem06 = (document.getElementById("dado06").value * 100) / document.getElementById("dado01").value;
                } else {
                    tmpDado06 = "0";
                    tmpPorcentagem06 = "0";
                }
                
                    if (document.forms[0].dado07) {
                    tmpDado07 = document.getElementById('dado07').value;
                    tmpPorcentagem07 = (document.getElementById("dado07").value * 100) / document.getElementById("dado01").value;
                } else {
                    tmpDado07 = "0";
                    tmpPorcentagem07 = "0";
                } 
                
                    if (document.forms[0].dado08) {
                    tmpDado08 = document.getElementById('dado08').value;
                    tmpPorcentagem08 = (document.getElementById("dado08").value * 100) / document.getElementById("dado01").value;
                } else {
                    tmpDado08 = "0";
                    tmpPorcentagem08 = "0";
                } 
                
                    if (document.forms[0].dado09) {
                    tmpDado09 = document.getElementById('dado09').value;
                    tmpPorcentagem09 = (document.getElementById("dado09").value * 100) / document.getElementById("dado01").value;
                } else {
                    tmpDado09 = "0";
                    tmpPorcentagem09 = "0";
                } 
                

                $("#grafico").modal("show");
                   
            $('#container').highcharts({
                            
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Casos Mais Comuns de Problema de Saúde e Doença'
                },
                subtitle: {
                    text: 'Fonte: UNA-SUS'
                },
                xAxis: {
                    categories: ['Dengue', 'Malária', 'Infecção Urinária', 'Hepatite viral B e C', 'Escalpelamento','Diarréia','Virose','Parasitose Instetinal'],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ''
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'bottom',
                    x: -40,
                    y: 100,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Porcentagem',
                    data: [tmpPorcentagem02, parseInt(tmpPorcentagem03), parseInt(tmpPorcentagem04), parseInt(tmpPorcentagem05), 
                          parseInt(tmpPorcentagem06), parseInt(tmpPorcentagem07), parseInt(tmpPorcentagem08), parseInt(tmpPorcentagem09)]
                }, 
                {  
                    name: 'Número',
                    data: [parseInt(tmpDado02), parseInt(tmpDado03), parseInt(tmpDado04), parseInt(tmpDado05), parseInt(tmpDado06), 
                          parseInt(tmpDado07), parseInt(tmpDado08), parseInt(tmpDado09)]
                }, 
                {
                    name: 'População',
                    data: [parseInt(tmpDado01), parseInt(tmpDado01), parseInt(tmpDado01), parseInt(tmpDado01), parseInt(tmpDado01), 
                          parseInt(tmpDado01), parseInt(tmpDado01), parseInt(tmpDado01)]
                    
                }]
            });
        }

       function Insere()
       {
           
     /* ### ALTERACAO REALIZADA AQUI ### */
      //document.inserir.action="inserir" ;
      //document.forms.inserir.submit();  
      document.forms[0].action="inserir" ;
      document.forms[0].submit();
    /* ### ALTERACAO REALIZADA AQUI ### */
     
       }
