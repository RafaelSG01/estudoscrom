// Setando namespace/módulo global
var florestas = {};

// Módulo do jogo
florestas.Game = (function() {

  // Inicialização de todos os componentes do jogo
  function Field() {
    var objects = 3;
    var canvas = document.getElementById("canvas");
    var stage  = new createjs.Stage(canvas);
    createjs.Touch.enable(stage);

    // Conteúdo
    var content = [{
      "id": 0,
      "question": "Assinale a alternativa que descreve doenças que relacionadas ao consumo de água poluída pode provocar na saúde humana:",
      "options": ["Cólera, desinteria e hepatite.",
      "Cólera, dengue e difteria.",
      "Cólera, hepatite e leishmaniose."],
      "feedback": ["Parabéns! Esta é a melhor alternativa de resposta à questão. Cólera, desinteria e hepatite são doenças de transmissão orofecal, que podem estar presentes em água não tratada.", "Esta não é a melhor alternativa de resposta à questão. A dengue é aedes aegypti e difteria é uma doença transmitida de pessoa para pessoa por contato físico ou respiratório.", "Esta não é a melhor alternativa de resposta à questão. A leishmaniose é uma doença transmitida por picada do mosquito palha."],
      "correct": 1,
      callback: function() {
        objects--;
        stage.removeChild(tipOne);
        success = new createjs.Bitmap(florestas.graphics.success.path);
        success.x = 132;
        success.y = 529;
        stage.addChild(success);
        updateStage();
      },
      callbackWrong: function() {
        endGame();
        stage.removeChild(tipOne);
        error = new createjs.Bitmap(florestas.graphics.error.path);
        error.x = 132;
        error.y = 529;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }, {
      "id": 1,
      "question": "Acidentes com picadas de serpentes não peçonhentas são relativamente frequentes, porém, não determinam acidentes graves, na maioria dos casos, e, por isso, são considerados de menor importância médica. O profissional de saúde deverá orientar a população, através de medidas educativas, que em casos de acidentes com animais peçonhentos, certas medidas devem ser adotadas, dentre as quais, é correto afirmar:",
      "options": ["Uso de torniquetes, sucção e incisão no local da picada.",
      "Não aplicar nenhum tipo de substâncias sobre o local da picada e procurar a unidade de saúde mais próxima.",
      "Dar bebidas alcoólicas ao acidentado, pois a mesma tem efeito contra o veneno."],     
      "feedback": ["Esta não é a melhor alternativa de resposta à questão. O uso de torniquetes pode provocar a amputação do membro picado, sucção do veneno é ineficiente e fazer uma incisão no local pode provocar uma infecção secundária.", "Parabéns! Esta é a melhor alternativa de resposta à questão.  O melhor a se fazer é tentar matar e capturar a serpente para que seja identificada no hospital e dirigir a vítima o mais rápido para o socorro.", "Esta não é a melhor alternativa de resposta à questão. Bebidas alcoólicas não são antídoto contra o veneno e podem, inclusive, potencializar a ação do veneno."],
      "correct": 2,
      callback: function() {
        objects--;
        stage.removeChild(tipTwo);
        success = new createjs.Bitmap(florestas.graphics.success.path);
        success.x = 334;
        success.y = 542;
        stage.addChild(success);
        updateStage();
        endGame();
      },
      callbackWrong: function() {
        stage.removeChild(tipTwo);
        error = new createjs.Bitmap(florestas.graphics.error.path);
        error.x = 334;
        error.y = 542;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }, {
      "id": 2,
      "question": "A doença de Chagas é uma doença tropical, muito comum em áreas rurais. Causada pelo Trypanosoma cruzi e transmitida por insetos conhecidos como barbeiros ou chupanças. Sobre a doença de Chagas, é correto afirmar:",
      "options": ["Cuidados com a conservação das casas, aplicação sistemática de inseticidas e utilização de telas em portas e janelas são algumas das medidas preventivas que devem ser adotadas, principalmente em ambientes rurais.",
      "A prevenção não é, em geral, focada em eliminar o inseto vetor com a utilização de sprays e tintas contendo inseticidas (piretroides sintéticos) e melhorar as condições de saneamento e habitação nas áreas rurais.",
      "A doença de Chagas é transmitida pelo contato de pessoa para pessoa, através de aperto de mãos, leite materno e relação sexual, bem como a ingestão de alimentos contaminados."],
      "feedback": ["Parabéns! Esta é a melhor alternativa de resposta à questão.  Os barbeiros gostam de manter seu habitat dentro de casa, em frestas na parede ou janelas.", "Esta não é a melhor alternativa de resposta à questão. A prevenção é focada na eliminação do inseto vetor por meio de inseticidas, telas e melhor conservação das casas. As condições de saneamento pouco influenciam no ciclo da transmissão.", "Esta não é a melhor alternativa de resposta à questão. A doença é transmitida através das fezes do barbeiro, que são liberadas no ato da picada e, ao coçar o local, o paciente inocula as fezes para dentro da ferida deixada pela picada."],
      "correct": 1,
      callback: function() {
        objects--;
        stage.removeChild(tipThree);
        success = new createjs.Bitmap(florestas.graphics.success.path);
        success.x = 740;
        success.y = 432;
        stage.addChild(success);
        updateStage();
        endGame();
      },
      callbackWrong: function() {
        stage.removeChild(tipThree);
        error = new createjs.Bitmap(florestas.graphics.error.path);
        error.x = 740;
        error.y = 432;
        stage.addChild(error);
        updateStage();
        objects--;
        endGame();
      }
    }];

    // Preloader
    function loadGraphics(e) {
      var imagesList = [
        {name:"tip", path:"assets/images/game/tip.png"},
        {name:"landscape", path:"assets/images/game/floresta.png"},
        {name:"success", path:"assets/images/game/success.png"},
        {name:"error", path:"assets/images/game/error.png"},
        {name:"end", path:"assets/images/game/end_floresta.png"},
      ]

      florestas.graphics = {};

      var totalFiles = imagesList.length;
      var loadedFiles = 0;
      for (var i=0, len=totalFiles; i<len; i++) {
        imageToLoad = imagesList[i];
        var img = new Image();
        img.onload = (function(event) {
          loadedFiles++;
          if (loadedFiles >= totalFiles) {
            initGame();
          }
        });
        img.src = imageToLoad.path;
        florestas.graphics[imageToLoad.name] = imageToLoad;
      };
    }

    // Função auxiliar para recarregar o palco
    var updateStage = function() {
      stage.update();
    }

    // Fim do jogo
    var endGame = function() {
      if (objects == 0) {
        stage.removeAllChildren();
        updateStage();
        var end = new createjs.Bitmap(florestas.graphics.end.path);
        stage.addChild(end);
        updateStage();
      }
    }

    var drawingTip = function() {
      // Rev Verde (1)
        tipOne = new createjs.Bitmap(florestas.graphics.tip.path);
        tipOne.x = 132;
        tipOne.y = 529;
        stage.addChild(tipOne);
        tipOne.addEventListener("click", function() {
          modal(content[0].id, content[0].feedback, content[0].question, content[0].options);
        });
      // Leito do rio
        tipTwo = new createjs.Bitmap(florestas.graphics.tip.path);
        tipTwo.x = 334;
        tipTwo.y = 542;
        stage.addChild(tipTwo);
        tipTwo.addEventListener("click", function() {
          modal(content[1].id, content[1].feedback, content[1].question, content[1].options);
        });
      // Rio
        tipThree = new createjs.Bitmap(florestas.graphics.tip.path);
        tipThree.x = 740;
        tipThree.y = 432;
        stage.addChild(tipThree);
        tipThree.addEventListener("click", function() {
          modal(content[2].id, content[2].feedback, content[2].question, content[2].options);
        });
    }

    var modal = function(id, feedback, question, options, correct) {
      this.id = id;
      this.question = question;
      this.options = options;
      this.feedback = feedback;
      var indexChar = 97;
      var str = "<form id='game_form_" + this.id + "'>";
      for(var i=0;i<this.options.length;i++){
        str += "<label class='choiceStyle' style='font-weight:100 !important;'><input type='radio' name='choice' value='" + (i + 1) + "'/> " + String.fromCharCode(indexChar++) + ") " + this.options[i] + "</label><br/>";
      }
      str += "<br><input type='button' class='btn btn-sm btn-warning' value='Confirmar escolha'/></form>";
      var customModal = $('<div class="custom-modal modal fade"><div class="modal-dialog"><div class="modal-content">' +
      '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
      '<h4 class="modal-title">Questão</h4></div>' +
      '<div class="modal-body"><div class="florestas-feedback"></div><p>'+ this.question +'</p>' +
      str +
      '</div>' +
      '</div></div></div>');

      $('body').append(customModal);
      $('.custom-modal').modal();

      $('.custom-modal').on('hidden.bs.modal', function(){
        $('.custom-modal').remove();
      });
      // Checando
      $("#game_form_"+ this.id +" input[type=button]").click({index: id}, function(e){
        var box = $(this).parent();
        var item = $(box).find("input[type=radio]:checked");
        var questionItem = $(item).val();
        var itemCorrect = content[e.data.index].correct;
        if(questionItem == undefined)
          return;

        if(questionItem == itemCorrect){
          $(".florestas-feedback").html('<div class="alert alert-success text-justify game-tip" role="alert">'+ feedback[questionItem-1] +'</div>');
          $('.custom-modal').on('hidden.bs.modal', function(){
            $('.custom-modal').remove();
            content[e.data.index].callback();
          });
        } else {
          $(".florestas-feedback").html('<div class="alert alert-danger text-justify game-tip" role="alert">'+ feedback[questionItem-1] +'</div>');
          $('.custom-modal').on('hidden.bs.modal', function(){
            $('.custom-modal').remove();
            content[e.data.index].callbackWrong();
          });
        }
        $(box).find('input').attr('disabled', true);
      });
    }

    // Fase inicial
    var initGame = function() {
      // Cenário
      var landscape = new createjs.Bitmap(florestas.graphics.landscape.path);
      landscape.y = 0;
      stage.addChild(landscape);
      drawingTip();
      // Atualizar palco para carregar os elementos na tela.
      updateStage();
    }
    loadGraphics();
  }
  return Field;
})();

// Chamando o jogo
$(function() {
  var game = new florestas.Game();
});